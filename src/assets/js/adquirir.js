/*<![CDATA[*/

	(function () {
	 var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
	 if (window.ShopifyBuy) {
		 if (window.ShopifyBuy.UI) {
			 ShopifyBuyInit();
		 } else {
			 loadScript();
		 }
	 } else {
		 loadScript();
	 }

	 function loadScript() {
		 var script = document.createElement('script');
		 script.async = true;
		 script.src = scriptURL;
		 (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
		 script.onload = ShopifyBuyInit;
	 }

	 function ShopifyBuyInit() {
		 var client = ShopifyBuy.buildClient({
			 domain: 'yaganaste.myshopify.com',
			 apiKey: '52ece40c6885684937529413c02229ab',
			 appId: '6',
		 });

		 ShopifyBuy.UI.onReady(client).then(function (ui) {
			 ui.createComponent('product', {
				 id: [130283405337],
				 node: document.getElementById('product-component-c9299f7d2dc'),
				 moneyFormat: '%24%20%7B%7Bamount%7D%7D',
				 options: {
	 "product": {
		 "buttonDestination": "checkout",
		 "layout": "horizontal",
		 "variantId": "all",
		 "width": "100%",
		 "contents": {
			 "img": false,
			 "imgWithCarousel": true,
			 "variantTitle": false,
			 "description": true,
			 "buttonWithQuantity": true,
			 "button": false,
			 "quantity": false
		 },
		 "text": {
			 "button": "Compra Ahora"
		 },
		 "styles": {
			 "product": {
				 "text-align": "left",
				 "@media (min-width: 601px)": {
					 "max-width": "100%",
					 "margin-left": "0",
					 "margin-bottom": "50px"
				 }
			 },
			 "button": {
				 "background-color": "#00a1e1",
				 ":hover": {
					 "background-color": "#0091cb"
				 },
				 ":focus": {
					 "background-color": "#0091cb"
				 }
			 },
			 "title": {
				 "font-size": "26px"
			 },
			 "price": {
				 "font-size": "18px"
			 },
			 "compareAt": {
				 "font-size": "15px"
			 }
		 }
	 },
	 "cart": {
		 "contents": {
			 "button": true
		 },
		 "styles": {
			 "button": {
				 "background-color": "#00a1e1",
				 ":hover": {
					 "background-color": "#0091cb"
				 },
				 ":focus": {
					 "background-color": "#0091cb"
				 }
			 },
			 "footer": {
				 "background-color": "#ffffff"
			 }
		 }
	 },
	 "modalProduct": {
		 "contents": {
			 "img": false,
			 "imgWithCarousel": true,
			 "variantTitle": false,
			 "buttonWithQuantity": true,
			 "button": false,
			 "quantity": false
		 },
		 "styles": {
			 "product": {
				 "@media (min-width: 601px)": {
					 "max-width": "100%",
					 "margin-left": "0px",
					 "margin-bottom": "0px"
				 }
			 },
			 "button": {
				 "background-color": "#00a1e1",
				 ":hover": {
					 "background-color": "#0091cb"
				 },
				 ":focus": {
					 "background-color": "#0091cb"
				 }
			 }
		 }
	 },
	 "toggle": {
		 "styles": {
			 "toggle": {
				 "background-color": "#00a1e1",
				 ":hover": {
					 "background-color": "#0091cb"
				 },
				 ":focus": {
					 "background-color": "#0091cb"
				 }
			 },
			 "count": {
				 "font-size": "16px"
			 }
		 }
	 },
	 "productSet": {
		 "styles": {
			 "products": {
				 "@media (min-width: 601px)": {
					 "margin-left": "-20px"
				 }
			 }
		 }
	 }
	}
			 });
		 });
	 }
	})();
	/*]]>*/
