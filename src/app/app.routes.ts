import { RouterModule, Routes} from '@angular/router'
import { HomeComponent } from './components/home/home.component';
import { AdquirirComponent } from './components/adquirir/adquirir.component';
import { FaqComponent } from './components/faq/faq.component';

const APP_ROUTES: Routes = [
	{ path: 'adquierelo', component: AdquirirComponent},
	{ path: 'preguntas-frecuentes', component: FaqComponent},
	{ path: '', component: HomeComponent },
	{ path: '**', pathMatch: 'full', redirectTo: '' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
