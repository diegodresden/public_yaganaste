import { Component, OnInit } from '@angular/core';
import { NumberOnlyDirective } from '../../directives/onlynumber.directive';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-ventajas',
  templateUrl: './ventajas.component.html',
  styles: []
})
export class VentajasComponent{

	values:number = 965.31;
	hint:boolean = true;
	track:boolean = false;
	compare:boolean = false;
	amount:boolean;
	viewmodal:boolean = false;

	constructor() {}

  onKey(event: any) {
    this.values = event.target.value;
    this.values = this.values * (1-(0.0299*1.16));
  }

  checkAmount(){
  	if (this.amount === null) { this.values = 965.31}
  }


	public closeModal($event) {
		if ($event.currentTarget === $event.target) {
			this.viewmodal = false;
		}
	}

}
