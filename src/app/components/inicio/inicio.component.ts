import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styles: []
})

export class InicioComponent {
	viewmodal:boolean = false;
	viewcontact:boolean = false;
	viewfaqs:boolean = false;

  constructor(private router:Router) {}

	public closeModal($event) {
		if ($event.currentTarget === $event.target) {
			this.viewmodal = false;
			this.viewcontact = false;
			this.viewfaqs = false;
		}
	}
}
