import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comenzar',
  templateUrl: './comenzar.component.html',
  styles: []
})
export class ComenzarComponent implements OnInit {
	viewmodal:boolean = false;
	howto_register:boolean = false;
	howto_pay:boolean = false;

  constructor() {}

  ngOnInit() {
  }

  public closeModal($event) {
		if ($event.currentTarget === $event.target) {
			this.viewmodal = false;
			this.howto_register = false;
			this.howto_pay = false;
		}
	}

}
