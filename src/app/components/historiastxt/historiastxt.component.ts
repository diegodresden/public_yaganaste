import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-historiastxt',
  templateUrl: './historiastxt.component.html',
  styles: []
})
export class HistoriastxtComponent implements OnInit {

  constructor() {}

  ngOnInit() {
  }

  viewmodal:boolean = false;
	public closeModal($event) {
		if ($event.currentTarget === $event.target) {
			this.viewmodal = false;
		}
	}

}
