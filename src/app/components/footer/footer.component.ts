import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styles: []
})
export class FooterComponent implements OnInit {
	viewcontact:boolean = false;
  constructor() {}

  ngOnInit() {
  }

  public closeModal($event) {
		if ($event.currentTarget === $event.target) {
			this.viewcontact = false;
		}
	}

}
