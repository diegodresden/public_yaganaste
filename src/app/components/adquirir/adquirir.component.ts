import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-adquirir',
  templateUrl: './adquirir.component.html',
  styles: []
})
export class AdquirirComponent implements OnInit {

  constructor(private router: Router) {
  	this.loadAPI = new Promise((resolve) => {
			this.loadScript();
			resolve(true);
    });
  }

  ngOnInit() {
  	this.router.events.subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) {
				return;
			}
				window.scrollTo(0, 0)
		});
  }

  loadAPI: Promise<any>;


		public loadScript() {
		    var isFound = false;
		    var scripts = document.getElementsByTagName("script")
		    for (var i = 0; i < scripts.length; ++i) {
		        if (scripts[i].getAttribute('src') != null && scripts[i].getAttribute('src').includes("loader")) {
		            isFound = true;
		        }
		    }

		    if (!isFound) {
		        var dynamicScripts = ["../../assets/js/adquirir.js"];

		        for (var i = 0; i < dynamicScripts .length; i++) {
		            let node = document.createElement('script');
		            node.src = dynamicScripts [i];
		            node.type = 'text/javascript';
		            node.async = false;
		            node.charset = 'utf-8';
		            document.getElementsByTagName('head')[0].appendChild(node);
		        }

		    }
		}

}
