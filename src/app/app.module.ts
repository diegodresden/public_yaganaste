import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'

import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { TestimonialComponent } from './components/testimonial/testimonial.component';
import { VentajasComponent } from './components/ventajas/ventajas.component';
import { HistoriastxtComponent } from './components/historiastxt/historiastxt.component';
import { ComenzarComponent } from './components/comenzar/comenzar.component';
import { FooterComponent } from './components/footer/footer.component';
import { FaqComponent } from './components/faq/faq.component';
import { AdquirirComponent } from './components/adquirir/adquirir.component';
import { HomeComponent } from './components/home/home.component';

//Directivas
import { Ng2TrackScrollModule } from 'ng2-track-scroll';

//Pipes
import { DomseguroPipe } from './pipes/domseguro.pipe';

//Rutas
import { APP_ROUTING } from './app.routes';
import { NumberOnlyDirective } from './directives/onlynumber.directive';


@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    TestimonialComponent,
    VentajasComponent,
    HistoriastxtComponent,
    ComenzarComponent,
    FooterComponent,
    DomseguroPipe,
    FaqComponent,
    AdquirirComponent,
    HomeComponent,
    NumberOnlyDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    APP_ROUTING,
    Ng2TrackScrollModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]

})
export class AppModule {}
